#ifndef CubeFX_Label_h
#define CubeFX_Label_h

#include "Standard.h"

class Shader;
class Texture;
class FontDescriptor;

class Label {
  
public:
  
  static Label* label(const std::string& text, const std::string& bitmap_font_name);
  
  void set_position(float x, float y);
  
  void render(const Shader& shader);
  
  inline void set_text(const std::string& text) { text_ = text; };
  
  void update(float dt);
  
private:
  
  Label() { };
  
  void init(const std::string& bitmap_font_name);
  
  glm::vec3 position_;
  std::string text_;
  
  Texture* texture_;
  FontDescriptor* font_descriptor_;
  
private:
  
  float fps_time_skipped_;
  
};

#endif
