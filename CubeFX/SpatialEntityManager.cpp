#include "SpatialEntityManager.h"

#include "EntityComponent.h"
#include "EntitySystem.h"
#include "EntityManagerUpdateJob.h"
#include "JobQueue.h"

SpatialEntityManager* SpatialEntityManager::manager() {
  return new SpatialEntityManager();
}

void SpatialEntityManager::init() {
}

void SpatialEntityManager::setup() {
  
}

void SpatialEntityManager::update() {
}