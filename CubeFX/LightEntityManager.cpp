#include "LightEntityManager.h"

#include "EntityComponent.h"
#include "EntitySystem.h"
#include "EntityManagerUpdateJob.h"
#include "JobQueue.h"

#include "LightEntitySetupJob.h"
#include "LightEntityUpdateJob.h"

static const char* LIGHT_KEY = "light";
static const char* SPATIAL_KEY = "spatial";

LightEntityManager* LightEntityManager::manager(GameScreen& game_screen) {
  return new LightEntityManager(game_screen);
}

void LightEntityManager::setup() {
  std::deque<EntityComponent*> components = EntitySystem::instance()->get_components(LIGHT_KEY);
  
  for (EntityComponent* component : components) {
    LightEntitySetupJob* setup_job = LightEntitySetupJob::job(game_screen, component);
    JobQueue::instance()->queue_job_on_main_thread(setup_job);
  }
}

void LightEntityManager::init() {
  EntityManagerUpdateJob* update_job = EntityManagerUpdateJob::job(this);
  JobQueue::instance()->queue_job(update_job);
}

void LightEntityManager::update() {
  std::deque<EntityComponent*> components = EntitySystem::instance()->get_components(LIGHT_KEY);
  
  for (EntityComponent* component : components) {
    EntityComponent* spatial = EntitySystem::instance()->get_component(component->id(), SPATIAL_KEY);
    LightEntityUpdateJob* update_job = LightEntityUpdateJob::job(game_screen, component, spatial);
    JobQueue::instance()->queue_job(update_job);
  }  

  EntityManagerUpdateJob* update_job = EntityManagerUpdateJob::job(this);
  JobQueue::instance()->queue_job(update_job);
}