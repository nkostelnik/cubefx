#include "LoadLevelJob.h"

#include "Standard.h"
#include "LevelFile.h"
#include "LevelFileEntity.h"
#include "EntitySystem.h"
#include "EntityManagement.h"

#include <deque>

LoadLevelJob* LoadLevelJob::job(const std::string& level_name, EntityManagement& entity_management) {
  return new LoadLevelJob(level_name, entity_management); 
}

void LoadLevelJob::run() {
  LOG("LoadLevelJob, loading level %s", level_name.c_str());
  
  LevelFile level_file(level_name);
  std::deque<EntityComponent*> components = level_file.components();
  std::deque<EntityComponent*>::iterator componentsIt = components.begin();
  
  for (; componentsIt != components.end(); ++componentsIt) {
    EntityComponent* component = (*componentsIt);
    EntitySystem::instance()->add_component(component);
  }
  
  entity_management.setup();
  
  LOG("LoadLevelJob, load level finished %s", level_name.c_str());
}