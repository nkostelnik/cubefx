#include "EntityManagerUpdateJob.h"

#include "EntityManager.h"

EntityManagerUpdateJob* EntityManagerUpdateJob::job(EntityManager* entity_manager) {
  return new EntityManagerUpdateJob(entity_manager);
}

void EntityManagerUpdateJob::run() {
  entity_manager->update();
}