#ifndef CubeFX_ThreadQueue_h
#define CubeFX_ThreadQueue_h

#include <queue>
#include <pthread.h>
#include "Standard.h"

class IJob;
class JobWorker;

class JobQueue {
  
public:
  
  static JobQueue* instance();
  
  void queue_job(IJob* job);
  
  void queue_job_on_main_thread(IJob* job);
  
  void update();
  
  void start_workers();
  
  IJob* next_job();
  
  IJob* next_main_job();
    
private:
  
  JobQueue();
  
  INT max_threads;
  
  std::queue<IJob*> waiting_jobs;
  std::queue<IJob*> waiting_main_thread_jobs;
  
  std::deque<JobWorker*> workers;
  
  pthread_mutex_t waiting_jobs_mutex;
  pthread_mutex_t main_waiting_jobs_mutex;
  
  static JobQueue* _instance;
  
};

#endif
