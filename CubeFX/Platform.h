#ifndef CubeFX_Platform_h
#define CubeFX_Platform_h

#include "Standard.h"

class Platform {
  
public:
  
  static INT ThreadCount();
  
  static std::string path_for_file(const char* file);
  
  static const FLOAT screen_width();
  
  static const FLOAT screen_height();
  
  static const FLOAT screen_orientation();
  
  static void load_image(const std::string& full_path, INT* width, INT* height, void** data);
  
  static float delta_time();
};

#endif
