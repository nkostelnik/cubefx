#ifndef CubeFX_LightEntitySetupJob_h
#define CubeFX_LightEntitySetupJob_h

#include "IJob.h"

class EntityComponent;
class GameScreen;

class LightEntitySetupJob : public IJob {
  
public:
  
  static LightEntitySetupJob* job(GameScreen& game_screen, const EntityComponent* component);
  
  LightEntitySetupJob(GameScreen& game_screen, const EntityComponent* component)
  : game_screen(game_screen)
  , component(component) { }
  
  void run();
  
private:
  
  const EntityComponent* component;
  GameScreen& game_screen;
  
};

#endif
