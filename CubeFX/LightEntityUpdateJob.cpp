#include "LightEntityUpdateJob.h"

#include "EntityComponent.h"
#include "GameScreen.h"

LightEntityUpdateJob* LightEntityUpdateJob::job(GameScreen& game_screen, const EntityComponent* component, const EntityComponent* spatial) {
  return new LightEntityUpdateJob(game_screen, component, spatial);
}

void LightEntityUpdateJob::set_light_position() {
  float x = spatial->float_attribute("x");
  float y = spatial->float_attribute("y");
  float z = spatial->float_attribute("z");
  glm::vec3 position(x, y, z);
  game_screen.set_light_position(component->id(), position);  
}

void LightEntityUpdateJob::set_light_color() {
  float r = component->float_attribute("r");
  float g = component->float_attribute("g");
  float b = component->float_attribute("b");
  glm::vec3 color(r, g, b);
//  game_screen.set_light_color(component->id(), color);    
}

void LightEntityUpdateJob::run() {  
  set_light_position();
  set_light_color();
}