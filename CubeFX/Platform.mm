#include "Platform.h"

#include <sys/sysctl.h>
#include <sys/time.h>

#include <Foundation/Foundation.h>

INT Platform::ThreadCount() {
  INT mib[4];
  INT numCPU = 0;
  size_t len = sizeof(numCPU); 
  
  /* set the mib for hw.ncpu */
  mib[0] = CTL_HW;
  mib[1] = HW_AVAILCPU;  // alternatively, try HW_NCPU;
  
  /* get the number of CPUs from the system */
  sysctl(mib, 2, &numCPU, &len, NULL, 0);
  
  if(numCPU < 1) 
  {
    mib[1] = HW_NCPU;
    sysctl( mib, 2, &numCPU, &len, NULL, 0 );
    
    if(numCPU < 1)
    {
      numCPU = 1;
    }
  }
  return (numCPU * 2) - 1;
}

std::string Platform::path_for_file(const char* file) {
  NSAutoreleasePool * pool = [[NSAutoreleasePool alloc] init];
  NSString* filename = [NSString stringWithUTF8String:file];
  
  NSString* path = [[NSBundle mainBundle] pathForResource:[[filename lastPathComponent] 
                    stringByDeletingPathExtension] ofType:[filename pathExtension]];
  
  
  
  std::string final_path = !path ? file : [path cStringUsingEncoding:NSUTF8StringEncoding];
    
  [pool release];
  return final_path;
}

const FLOAT Platform::screen_width() {
  UIDeviceOrientation orientation = [[UIDevice currentDevice] orientation];

  if (orientation == UIDeviceOrientationLandscapeLeft || orientation == UIDeviceOrientationLandscapeRight) {
    return [UIScreen mainScreen].bounds.size.height;
  }
    
  return [UIScreen mainScreen].bounds.size.width;
}

const FLOAT Platform::screen_height() {
  UIDeviceOrientation orientation = [[UIDevice currentDevice] orientation];
  
  if (orientation == UIDeviceOrientationLandscapeLeft || orientation == UIDeviceOrientationLandscapeRight) {
    return [UIScreen mainScreen].bounds.size.width;
  }
  
  return [UIScreen mainScreen].bounds.size.height;
}

const FLOAT Platform::screen_orientation() {
  UIDeviceOrientation orientation = [[UIDevice currentDevice] orientation];
  
  float angle = 0.0f;
  
  if (orientation == UIDeviceOrientationLandscapeLeft) {
    angle = 90;
  }
  
  if (orientation == UIDeviceOrientationLandscapeRight) {
    angle = -90;
  }
  
  if (orientation == UIDeviceOrientationPortraitUpsideDown) {
    angle = 180;
  }
  
  return angle;
}

void Platform::load_image(const std::string& full_path, int *width, int *height, void **data) {
  NSString* path = [NSString stringWithUTF8String:full_path.c_str()];  
  NSData *texData = [[NSData alloc] initWithContentsOfFile:path];
  UIImage *image = [[UIImage alloc] initWithData:texData];
  if (image == nil)
    NSLog(@"Do real error checking here");
  
  *width = CGImageGetWidth(image.CGImage);
  *height = CGImageGetHeight(image.CGImage);
  CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
  *data = malloc(*height * *width * 4);
  CGContextRef context = CGBitmapContextCreate(*data, *width, *height, 8, 4 * *width, colorSpace, kCGImageAlphaPremultipliedLast | kCGBitmapByteOrder32Big );
  CGColorSpaceRelease(colorSpace);

  CGRect bounds = CGRectMake(0, 0, *width, *height) ;
  CGContextClearRect(context, bounds);
  CGContextTranslateCTM(context, 0, *height);
  CGContextScaleCTM(context, 1.0, -1.0);
  CGContextDrawImage(context, bounds, image.CGImage);
  CGContextRelease(context);
}

float Platform::delta_time() {
  static struct timeval last_update_;
  struct timeval now;
  float dt = 0.0f;
  
  if(gettimeofday(&now, NULL) != 0) {
    dt = 0;
    return dt;
  }
  
  dt = (now.tv_sec - last_update_.tv_sec) + (now.tv_usec - last_update_.tv_usec) / 1000000.0f;
  last_update_ = now;	
  return dt;
}
