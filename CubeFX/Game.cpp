#include "Game.h"

#include "Standard.h"
#include "GameScreen.h"
#include "LoadLevelJob.h"
#include "RenderJob.h"

Game::Game() : is_init_(false) {
  entity_management = EntityManagement(game_screen);
}

void Game::init() {  
  is_init_ = true;
  
  JobQueue::instance()->start_workers();
  
  game_screen.init();
  ui.init();
  entity_management.init();
  
  LoadLevelJob* load_level_job = LoadLevelJob::job("level1.json", entity_management);
  JobQueue::instance()->queue_job(load_level_job);
}

void Game::main_loop() {
  glClearColor(1, 0, 1, 1);
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
  float dt = clock.delta_time();
  ui.update(dt);
  game_screen.render();
  ui.render();
  JobQueue::instance()->update();
}