#ifndef CubeFX_LevelFile_h
#define CubeFX_LevelFile_h

#include "Standard.h"

#include "EntityComponent.h"

class LevelFile {
  
public:
  
  LevelFile(const std::string& file_path)
  : file_path(file_path) { };
  
  std::deque<EntityComponent*> components();
  
private:
  
  std::string file_path;
  
};

#endif
