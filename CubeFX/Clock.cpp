#include "Clock.h"

#include "Platform.h"

float Clock::delta_time() {
  float dt = Platform::delta_time();
  
  if (first_run_) {
    first_run_ = false;
    dt = 0;
  }
  
  return dt;
}
