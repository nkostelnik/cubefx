uniform vec4 ambientColor;
uniform vec4 diffuseColor;
uniform vec4 specularColor;

varying vec3 varyingNormal;
varying vec3 varyingLightDir;

//uniform vec3 lightPosition[];
//uniform int lightCount;

void main() {
  float diffuse = max(0.0, dot(normalize(varyingNormal),
                               normalize(varyingLightDir)));

  gl_FragColor = diffuseColor * diffuse;
}
