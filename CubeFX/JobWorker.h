#ifndef CubeFX_JobWorker_h
#define CubeFX_JobWorker_h

#include "Standard.h"

class JobQueue;

struct JobWorkerArgs {
  
  JobWorkerArgs(JobQueue* queue, INT id) 
  : queue(queue), id(id) { };
  
  JobQueue* queue;
  INT id;
  
};

class JobWorker {
  
public:
  
  static void* worker(void* args);
  
  JobWorker(JobQueue* queue, INT id)
  : queue(queue), id(id), working(true) { };
  
  void start_work();
  
private:
  
  JobQueue* queue;
  INT id;
  BOOLEAN working;
    
};

#endif
