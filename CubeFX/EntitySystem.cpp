#include "EntitySystem.h"

#include "EntityComponent.h"

const char* TYPE_KEY = "type";

EntitySystem* EntitySystem::_instance = 0;

EntitySystem* EntitySystem::instance() {
  if (!_instance) {
    _instance = new EntitySystem();
  }
  return _instance;
}

void EntitySystem::add_component(EntityComponent* component) {
  components.push_back(component);
}

std::deque<EntityComponent*> EntitySystem::get_components(const char* type) const {
  std::deque<EntityComponent*> results;
  std::deque<EntityComponent*>::const_iterator componentsIt = components.begin();
  for (; componentsIt != components.end(); ++componentsIt) {
    EntityComponent* component = (*componentsIt);
    if (component->string_attribute(TYPE_KEY).compare(std::string(type)) == 0) {
      results.push_back(component);
    }
  }
  return results;
}

EntityComponent* EntitySystem::get_component(INT id, const char* type) const {
  std::deque<EntityComponent*> results = get_components(type);
  for (EntityComponent* component : results) {
    if (component->id() == id) {
      return component;
    }
  }
  return 0;
}