#ifndef CubeFX_MeshResource_h
#define CubeFX_MeshResource_h

#include "Standard.h"

class MaterialResource;

class MeshResource {
  
public:
  
  MeshResource(float* vertices, int vertex_count, int vertex_size,
               float* normals, int normal_count, int normal_size,
               float* uvs, int uv_count, int uv_size,
               const glm::vec3& translation, const glm::vec3& rotation, const glm::vec3& scale,
               std::deque<MaterialResource*> materials)
  : vertices(vertices)
  , vertex_count(vertex_count)
  , vertex_size(vertex_size)

  , normals(normals)
  , normal_count(normal_count)
  , normal_size(normal_size)

  , uvs(uvs)
  , uv_count(uv_count)
  , uv_size(uv_size)

  , translation(translation)
  , rotation(rotation)
  , scale(scale)
  , materials(materials) {
    
  }
  
  FLOAT* vertices;
  INT vertex_count;
  INT vertex_size;

  FLOAT* normals;
  INT normal_count;
  INT normal_size;

  FLOAT* uvs;
  INT uv_count;
  INT uv_size;
  
  glm::vec3 translation;
  glm::vec3 rotation;
  glm::vec3 scale;
  std::deque<MaterialResource*> materials;
  
};

#endif
