#ifndef CubeFX_EntitySetupJob_h
#define CubeFX_EntitySetupJob_h

#include "IJob.h"

class EntityManager;

class EntitySetupJob : public IJob {
  
public:
  
  static EntitySetupJob* job(EntityManager* entity_manager);
  
  EntitySetupJob(EntityManager* entity_manager)
  : entity_manager(entity_manager) { };
  
  void run();
  
private:
  
  EntityManager* entity_manager;
  
};

#endif
