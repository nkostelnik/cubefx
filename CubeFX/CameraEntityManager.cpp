#include "CameraEntityManager.h"

static const char* CAMERA_KEY = "camera";
static const char* SPATIAL_KEY = "spatial";

#include "EntitySystem.h"
#include "CameraEntitySetupJob.h"
#include "JobQueue.h"
#include "EntityManagerUpdateJob.h"
#include "CameraEntityUpdateJob.h"

CameraEntityManager* CameraEntityManager::manager(GameScreen& game_screen) {
  return new CameraEntityManager(game_screen);
}

void CameraEntityManager::setup() {
  std::deque<EntityComponent*> components = EntitySystem::instance()->get_components(CAMERA_KEY);
  
  for (EntityComponent* component : components) {
    CameraEntitySetupJob* setup_job = CameraEntitySetupJob::job(game_screen, component);
    JobQueue::instance()->queue_job_on_main_thread(setup_job);
  }

}

void CameraEntityManager::init() {
  EntityManagerUpdateJob* update_job = EntityManagerUpdateJob::job(this);
  JobQueue::instance()->queue_job(update_job);  
}

void CameraEntityManager::update() {
  std::deque<EntityComponent*> components = EntitySystem::instance()->get_components(CAMERA_KEY);
  
  for (EntityComponent* component : components) {
    EntityComponent* spatial = EntitySystem::instance()->get_component(component->id(), SPATIAL_KEY);
    CameraEntityUpdateJob* update_job = CameraEntityUpdateJob::job(game_screen, component, spatial);
    JobQueue::instance()->queue_job(update_job);
  }  
  
  EntityManagerUpdateJob* update_job = EntityManagerUpdateJob::job(this);
  JobQueue::instance()->queue_job(update_job);
}
