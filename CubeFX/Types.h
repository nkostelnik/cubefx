#ifndef CubeFX_Types_h
#define CubeFX_Types_h

#define INT int
#define UINT unsigned int
#define BOOLEAN bool
//#define LONG long
#define CHAR char
#define DOUBLE double
#define FLOAT float

#include <inttypes.h>
#define BYTE uint8_t
#define WORD uint16_t
#define DWORD uint32_t
#define LONG int32_t

#define INVALID -1

#endif
