#ifndef CubeFX_RenderJob_h
#define CubeFX_RenderJob_h

#include "IJob.h"

class GameScreen;
class UI;

class RenderJob : public IJob {

public:
  
  static RenderJob* job(const GameScreen& game_screen, const UI& ui);
  
  RenderJob(const GameScreen& game_screen, const UI& ui)
  : game_screen(game_screen)
  , ui(ui) { };
  
  void run();
  
private:
  
  const GameScreen& game_screen;
  const UI& ui;
  
};


#endif
