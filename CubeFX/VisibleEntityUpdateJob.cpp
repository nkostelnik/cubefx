#include "VisibleEntityUpdateJob.h"

#include "EntityComponent.h"
#include "GameScreen.h"

VisibleEntityUpdateJob* VisibleEntityUpdateJob::job(GameScreen& game_screen, const EntityComponent* component) {
  return new VisibleEntityUpdateJob(game_screen, component);
}

void VisibleEntityUpdateJob::run() {
  
  float x = component->float_attribute("x");
  float y = component->float_attribute("y");
  float z = component->float_attribute("z");
  glm::vec3 position(x, y, z);
  game_screen.set_model_position(component->id(), position);
}