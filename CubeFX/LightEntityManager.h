#ifndef CubeFX_LightEntityManager_h
#define CubeFX_LightEntityManager_h

#include "EntityManager.h"

class GameScreen;

class LightEntityManager : public EntityManager {
  
public:
  
  static LightEntityManager* manager(GameScreen& game_screen);
  
  LightEntityManager(GameScreen& game_screen)
  : game_screen(game_screen) { };
  
  void setup();
  
  void init();
  
  void update();
  
private:
  
  GameScreen& game_screen;
  
};

#endif
