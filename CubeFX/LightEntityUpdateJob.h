#ifndef CubeFX_LightEntityUpdateJob_h
#define CubeFX_LightEntityUpdateJob_h

#include "IJob.h"

class EntityComponent;
class GameScreen;

class LightEntityUpdateJob : public IJob {

public:
  
  static LightEntityUpdateJob* job(GameScreen& game_screen, const EntityComponent* component, const EntityComponent* spatial);
  
  LightEntityUpdateJob(GameScreen& game_screen, const EntityComponent* component, const EntityComponent* spatial)
  : game_screen(game_screen)
  , component(component) 
  , spatial(spatial) { }
  
  void run();
  
private:
  
  void set_light_position();
  
  void set_light_color();
  
  const EntityComponent* component;
  const EntityComponent* spatial;
  
  GameScreen& game_screen;

};

#endif
