#include "FontDescriptorResource.h"

#include "ResourceCache.h"
#include "Platform.h"

FontDescriptorResource* FontDescriptorResource::resource() {
  return new FontDescriptorResource();
}

void FontDescriptorResource::load(const std::string& path) {
  std::string full_path = Platform::path_for_file(path.c_str());
  data_ = File(full_path).data();
}