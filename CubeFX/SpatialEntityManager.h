#ifndef CubeFX_SpatialEntityManager_h
#define CubeFX_SpatialEntityManager_h

#include "EntityManager.h"

class SpatialEntityManager : public EntityManager {
  
public:
  
  static SpatialEntityManager* manager();
  
  void setup();
  
  void update();
  
  void init();
  
};

#endif
