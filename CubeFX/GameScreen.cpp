#include "GameScreen.h"

#include "Standard.h"
#include "Platform.h"
#include "glm/gtc/matrix_transform.hpp"

#include "Model.h"
#include "Light.h"
#include "Camera.h"

#include "Shader.h"
#include "ResourceCache.h"
#include "ShaderAttribs.h"

void GameScreen::add_model(Model* model) {
  models.push_back(model);
}

void GameScreen::add_light(Light *light) {
  lights.push_back(light);
}

void GameScreen::add_camera(Camera* camera) {
  cameras.push_back(camera);
}

void GameScreen::set_active_camera(Camera* camera) {
  for (Camera* camera_b : cameras) {
    camera_b->set_active(false);
    if (camera == camera_b) {
      camera_b->set_active(true);
    }
  }
}

void GameScreen::init() {
  glEnable(GL_DEPTH_TEST);
  glEnable(GL_CULL_FACE);
    
  ShaderResource* shader_resource = ResourceCache::load_shader("");
  
  shader = Shader::shader();
  shader->compile_vertex(shader_resource->vertex_source());
  shader->compile_fragment(shader_resource->fragment_source());
  
  shader->bind_attribute(ATTRIB_VERTEX, "vertex");
  shader->bind_attribute(ATTRIB_NORMAL, "normal");
  shader->bind_attribute(ATTRIB_UV, "textureCoords");

  shader->link();
  
  shader->add_uniform("projection");
  shader->add_uniform("modelview");
  shader->add_uniform("normalMatrix");
  shader->add_uniform("ambientColor");
  shader->add_uniform("diffuseColor");
  shader->add_uniform("specularColor");
  shader->add_uniform("lightPosition");
  shader->add_uniform("lightCount");
}

void GameScreen::render() const {    
  glm::mat4 screen_orientation = glm::rotate(glm::mat4(1.0f), -Platform::screen_orientation(), glm::vec3(0.0f, 0.0f, 1.0f));
  glm::mat4 projection = glm::perspective(60.0f, Platform::screen_width() / Platform::screen_height(), 0.5f, 200.0f);
  projection = screen_orientation * projection;
  
  glm::mat4 view(1.0f);
  
  for (Camera* camera : cameras) {
    if (camera->is_active()) {
      view = glm::translate(glm::mat4(1.0f), -camera->position);
    }
  }
  
  shader->use();
  shader->set_uniform(projection, "projection");
  shader->set_uniform(lights, "lightPosition");
  shader->set_uniform(lights.size(), "lightCount");
  
  std::deque<Model*>::const_iterator modelsIt = models.begin();
  for (; modelsIt != models.end(); ++modelsIt) {
    (*modelsIt)->render(*shader, view);
  }  
}

void GameScreen::set_light_position(INT id, const glm::vec3& position) {
  for (Light* light : lights) {
    if (light->id == id) {
      light->position = position;
    }
  }
}

void GameScreen::set_model_position(INT id, const glm::vec3& position) {
  for (Model* model : models) {
    if (model->id == id) {
      model->position = position;
    }
  }
}

void GameScreen::set_camera_position(INT id, const glm::vec3& position) {
  for (Camera* camera : cameras) {
    if (camera->id == id) {
      camera->position = position;
    }
  }
}