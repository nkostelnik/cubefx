#ifndef CubeFX_Standard_h
#define CubeFX_Standard_h

#include "Types.h"
#include "Log.h"

#include <string>
#include <sstream>
#include <deque>
#include <vector>
#include <map>
#include <stack>

#import <OpenGLES/ES2/gl.h>
#import <OpenGLES/ES2/glext.h>

#include "glm/glm.hpp"
#include "glm/gtx/projection.hpp"
#include "glm/gtc/matrix_transform.hpp"
#include "glm/gtc/matrix_inverse.hpp"
#include "glm/gtc/type_ptr.hpp"

#endif
