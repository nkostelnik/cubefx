#include "VisibleEntitySetupJob.h"

#include "EntityComponent.h"
#include "ResourceCache.h"
#include "Model.h"

#include "GameScreen.h"

VisibleEntitySetupJob* VisibleEntitySetupJob::job(GameScreen& game_screen, const EntityComponent* component) {
  return new VisibleEntitySetupJob(game_screen, component);
}

void VisibleEntitySetupJob::run() {
  static const char* MESH_KEY = "mesh";
  std::string mesh = component->string_attribute(MESH_KEY);
  ModelResource* mesh_resource = ResourceCache::load_model(mesh);
    
  Model* model = Model::model(component->id(), mesh_resource);
  game_screen.add_model(model);
}
