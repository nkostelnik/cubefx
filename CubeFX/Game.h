#ifndef CubeFX_Game_h
#define CubeFX_Game_h

#include "JobQueue.h"
#include "EntityManagement.h"
#include "GameScreen.h"
#include "UI.h"
#include "Clock.h"

class Game {
  
public:
  
  Game();
  
  void init();
  
  inline bool is_init() { return is_init_; };
  
  void main_loop();
  
private:

  Clock clock;
  GameScreen game_screen;
  UI ui;
  EntityManagement entity_management;
  bool is_init_;
  
};

#endif
