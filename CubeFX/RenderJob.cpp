#include "RenderJob.h"

#include "JobQueue.h"
#include "GameScreen.h"
#include "UI.h"

RenderJob* RenderJob::job(const GameScreen& game_screen, const UI& ui) {
  return new RenderJob(game_screen, ui);
}

void RenderJob::run() { 
  glClearColor(1, 0, 1, 1);
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
  game_screen.render();
  ui.render();
  RenderJob* render_job = RenderJob::job(game_screen, ui);
  JobQueue::instance()->queue_job_on_main_thread(render_job);
}