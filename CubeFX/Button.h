#ifndef CubeFX_Button_h
#define CubeFX_Button_h

#include "Standard.h"

class Shader;

class Button {
  
public:
  
  static Button* button();
  
  void set_position(float x, float y);
  
  void render(const Shader& shader);
  
private:
  
  Button() { };
  
  glm::vec3 position_;
  
};

#endif
