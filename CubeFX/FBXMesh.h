#ifndef CubeFX_FBXMesh_h
#define CubeFX_FBXMesh_h

#include "Standard.h"

struct FBXMesh {
  
public:
  
  FLOAT* vertices;
  INT vertex_count;
  INT vertex_size;
  
};

#endif
