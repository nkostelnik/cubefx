#ifndef CubeFX_ResourceCache_h
#define CubeFX_ResourceCache_h

#include "ModelResource.h"
#include "ShaderResource.h"
#include "FontDescriptorResource.h"
#include "TextureResource.h"
#include <string>

class ResourceCache {
  
public:
  
  static ModelResource* load_model(const std::string& resource_file);
  
  static ShaderResource* load_shader(const std::string& resource_file);
  
  static FontDescriptorResource* load_font_descriptor(const std::string& resource_file);
  
  static TextureResource* load_texture(const std::string& resource_file);
  
};

#endif
