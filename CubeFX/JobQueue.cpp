#include "JobQueue.h"

#include <pthread.h>

#include "Platform.h"
#include "IJob.h"
#include "JobWorker.h"

JobQueue* JobQueue::_instance = 0;

JobQueue* JobQueue::instance() { 
  if (!_instance) {
    _instance = new JobQueue();
  }
  return _instance;
}

JobQueue::JobQueue() {
  pthread_mutex_init(&waiting_jobs_mutex, 0);
  pthread_mutex_init(&main_waiting_jobs_mutex, 0);
  max_threads = Platform::ThreadCount();    
  LOG("Job Queue, possible threads to use: %d", max_threads);
}

void JobQueue::queue_job(IJob* job) {
  pthread_mutex_lock(&waiting_jobs_mutex);
  waiting_jobs.push(job);
  pthread_mutex_unlock(&waiting_jobs_mutex);
}

void JobQueue::queue_job_on_main_thread(IJob *job) {
  pthread_mutex_lock(&main_waiting_jobs_mutex);
  waiting_main_thread_jobs.push(job);
  pthread_mutex_unlock(&main_waiting_jobs_mutex);
}

void JobQueue::update() {
  if (!max_threads) {
    IJob* job = this->next_job();
    if(job) {
      job->run();
      delete job;
    }
  }
  
  IJob* job = next_main_job();
  if (job) {
    job->run(); 
    delete job;
  }
}

void JobQueue::start_workers() {
  pthread_t threads[max_threads];
  for (INT i = 0; i < max_threads; i++) {
    JobWorkerArgs* worker_args = new JobWorkerArgs(this, i);
    pthread_create(&threads[i], NULL, JobWorker::worker, worker_args);
  }
}

IJob* JobQueue::next_job() {
  pthread_mutex_lock(&waiting_jobs_mutex);
  IJob* job = 0;
  if (waiting_jobs.size()) {
    job = waiting_jobs.front();
    waiting_jobs.pop();
  }
  pthread_mutex_unlock(&waiting_jobs_mutex);
  return job;
}

IJob* JobQueue::next_main_job() {
  pthread_mutex_lock(&main_waiting_jobs_mutex);
  IJob* job = 0;
  if (waiting_main_thread_jobs.size()) {
    job = waiting_main_thread_jobs.front();
    waiting_main_thread_jobs.pop();
  }
  pthread_mutex_unlock(&main_waiting_jobs_mutex);
  return job;
}