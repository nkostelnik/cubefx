#ifndef CubeFX_MatrixStack_h
#define CubeFX_MatrixStack_h

#include "Standard.h"

class MatrixStack {
      
public:
  
  void push(const glm::mat4& matrix);
  
  glm::mat4 concatenate();
  
private:
  
  std::deque<glm::mat4> stack;
  
};

#endif
