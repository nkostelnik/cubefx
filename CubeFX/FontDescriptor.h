#ifndef CubeFX_FontDescriptor_h
#define CubeFX_FontDescriptor_h

#include "Standard.h"

struct FontCharacterInfo {
  
  INT width;
  INT height;
  INT x;
  INT y;
  INT char_id;
  INT offset_x;
  INT offset_y;
};

class FontDescriptorResource;

class FontDescriptor {
  
public:
  
  static FontDescriptor* descriptor(FontDescriptorResource* font_descriptor_resource);
  
  FontCharacterInfo char_info(char character);
  
  void add_char_info(const FontCharacterInfo& char_info);
  
  inline float width() { return width_; }
  
  inline float height() { return height_; }
  
private:
  
  FontDescriptor() { };
    
  std::map<char, FontCharacterInfo> char_info_;
  
  float width_;
  float height_;
  
};

#endif
