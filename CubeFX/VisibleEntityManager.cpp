#include "VisibleEntityManager.h"

#include "EntityComponent.h"
#include "EntitySystem.h"
#include "EntityManagerUpdateJob.h"
#include "JobQueue.h"

#include "VisibleEntitySetupJob.h"
#include "VisibleEntityUpdateJob.h"

#include "ResourceCache.h"
#include "ModelResource.h"

static const char* VISIBLE_KEY = "visible";
static const char* SPATIAL_KEY = "spatial";

VisibleEntityManager* VisibleEntityManager::manager(GameScreen& game_screen) {
  return new VisibleEntityManager(game_screen);
}

void VisibleEntityManager::setup() {
  std::deque<EntityComponent*> components = EntitySystem::instance()->get_components(VISIBLE_KEY);
  
  for (EntityComponent* component : components) {
    VisibleEntitySetupJob* setup_job = VisibleEntitySetupJob::job(game_screen, component);
    JobQueue::instance()->queue_job(setup_job);
  }
}

void VisibleEntityManager::init() {
  EntityManagerUpdateJob* update_job = EntityManagerUpdateJob::job(this);
  JobQueue::instance()->queue_job(update_job);
}

void VisibleEntityManager::update() {
  std::deque<EntityComponent*> components = EntitySystem::instance()->get_components(SPATIAL_KEY);
  
  for (EntityComponent* component : components) {
    VisibleEntityUpdateJob* update_job = VisibleEntityUpdateJob::job(game_screen, component);
    JobQueue::instance()->queue_job(update_job);
  }  
  
  EntityManagerUpdateJob* update_job = EntityManagerUpdateJob::job(this);
  JobQueue::instance()->queue_job(update_job);
}