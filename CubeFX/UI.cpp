#include "UI.h"

#include "Standard.h"
#include "Platform.h"

#include "TextureResource.h"
#include "Texture.h"

#include "Shader.h"
#include "ShaderResource.h"

#include "ShaderAttribs.h"

#include "Button.h"
#include "Label.h"

void UI::init() {  
  ShaderResource* shader_resource = ShaderResource::resource();
  shader_resource->load("ui.vsh", "ui.fsh");
  
  shader = Shader::shader();
  shader->compile_vertex(shader_resource->vertex_source());
  shader->compile_fragment(shader_resource->fragment_source());
  
  shader->bind_attribute(ATTRIB_VERTEX, "vertex");
  shader->bind_attribute(ATTRIB_UV, "textureCoords");
  shader->link();
  
  shader->add_uniform("model");
  shader->add_uniform("projection");
    
  Label* label = Label::label("", "copperplate_18");
  labels.push_back(label);
}

void UI::render() const {
  glDisable(GL_DEPTH_TEST);
  glEnable(GL_BLEND);
  shader->use();  
  glm::mat4 gui_projection = glm::ortho(0.0f, Platform::screen_width(), 0.0f, Platform::screen_height(), 1.0f, -1.0f);
  glm::mat4 screen_orientation = glm::rotate(glm::mat4(1.0f), -Platform::screen_orientation(), glm::vec3(0.0f, 0.0f, 1.0f));
  gui_projection =  screen_orientation * gui_projection;
  shader->set_uniform(gui_projection, "projection");  

  for (Button* button : buttons) {
    button->render(*shader);
  }
  
  for (Label* label : labels) {
    label->render(*shader);
  }
  
  glEnable(GL_DEPTH_TEST);
  glDisable(GL_BLEND);
}

void UI::update(float dt) {
  for (Label* label : labels) {
    label->update(dt);
  }
}