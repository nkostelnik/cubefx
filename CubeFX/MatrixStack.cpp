#include "MatrixStack.h"

void MatrixStack::push(const glm::mat4& matrix) {
  stack.push_back(matrix);
}

glm::mat4 MatrixStack::concatenate() {
  glm::mat4 concatenated(1.0f);
  for (glm::mat4& item : stack) {
    concatenated *= item;
  }  
  return concatenated;
}
