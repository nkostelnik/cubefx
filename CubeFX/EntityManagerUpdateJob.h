#ifndef CubeFX_EntityManagerUpdateJob_h
#define CubeFX_EntityManagerUpdateJob_h

#include "IJob.h"

class EntityManager;

class EntityManagerUpdateJob : public IJob {
  
public:
  
  static EntityManagerUpdateJob* job(EntityManager* entity_manager);
  
  EntityManagerUpdateJob(EntityManager* entity_manager)
  : entity_manager(entity_manager) { };
  
  void run();
  
private:
  
  EntityManager* entity_manager;
  
};

#endif
