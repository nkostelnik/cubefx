#import <QuartzCore/QuartzCore.h>

#import "CubeFXViewController.h"
#import "EAGLView.h"

#include "Game.h"

// Uniform index.
enum {
    UNIFORM_TRANSLATE,
    NUM_UNIFORMS
};
GLint uniforms[NUM_UNIFORMS];

// Attribute index.
enum {
    ATTRIB_VERTEX,
    ATTRIB_COLOR,
    NUM_ATTRIBUTES
};

@interface CubeFXViewController ()
@property (nonatomic, retain) EAGLContext *context;
@end

@implementation CubeFXViewController

@synthesize animating;
@synthesize context;

- (void)awakeFromNib {
  EAGLContext *aContext = [[EAGLContext alloc] initWithAPI:kEAGLRenderingAPIOpenGLES2];
  
  if (!aContext) {
    aContext = [[EAGLContext alloc] initWithAPI:kEAGLRenderingAPIOpenGLES1];
  }
  
  if (!aContext) {
    NSLog(@"Failed to create ES context");
  }
  else if (![EAGLContext setCurrentContext:aContext]) {
    NSLog(@"Failed to set ES context current");
  }
    
	self.context = aContext;
	[aContext release];
	
  [(EAGLView *)self.view setContext:context];
  [(EAGLView *)self.view setFramebuffer];
  
  animating = FALSE;
  displayLink = nil;
}

- (void)dealloc {
  // Tear down context.
  if ([EAGLContext currentContext] == context)
      [EAGLContext setCurrentContext:nil];
  
  [context release];
  
  [super dealloc];
}

- (void)didReceiveMemoryWarning {
  [super didReceiveMemoryWarning];    
  NSLog(@"################ MEMORY WARNING ###################");
}

- (void)viewWillAppear:(BOOL)animated {
  [self startAnimation];  
  [super viewWillAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated {
  [self stopAnimation];  
  [super viewWillDisappear:animated];
}

- (void)viewDidUnload {
	[super viewDidUnload];
	
  // Tear down context.
  if ([EAGLContext currentContext] == context)
      [EAGLContext setCurrentContext:nil];

	self.context = nil;	
}

- (void)startAnimation {
    if (!animating) {
        CADisplayLink *aDisplayLink = [[UIScreen mainScreen] displayLinkWithTarget:self selector:@selector(mainLoop)];
        [aDisplayLink addToRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
        displayLink = aDisplayLink;
        animating = TRUE;
    }
}

- (void)stopAnimation {
    if (animating) {
        [displayLink invalidate];
        displayLink = nil;
        animating = FALSE;
    }
}

- (void)mainLoop {
  if (!game.is_init()) {
    game.init();
  }
  [(EAGLView *)self.view setFramebuffer];  
  game.main_loop();
  [(EAGLView *)self.view presentFramebuffer];
}

@end
