#include "EntityComponent.h"

#include "Any.h"
#include <string>

EntityComponent* EntityComponent::component(INT component_id) {
  return new EntityComponent(component_id);
}

void EntityComponent::set_attribute(const std::string& key, FLOAT value) {
  float_attributes[key] = value;
}

void EntityComponent::set_attribute(const std::string& key, INT value) {
  int_attributes[key] = value;
}

void EntityComponent::set_attribute(const std::string& key, const std::string& value) {
  string_attributes[key] = value;
}

void EntityComponent::set_attribute(const std::string& key, bool value) {
  bool_attributes[key] = value;
}

std::string EntityComponent::string_attribute(const std::string& key) const {
  std::map<std::string, std::string>::const_iterator attributesIt = string_attributes.find(key);
  return (attributesIt != string_attributes.end()) ? (*attributesIt).second : "";
}

float EntityComponent::float_attribute(const std::string& key) const {
  std::map<std::string, FLOAT>::const_iterator attributesIt = float_attributes.find(key);
  return (attributesIt != float_attributes.end()) ? (*attributesIt).second : 0;
}

float EntityComponent::int_attribute(const std::string& key) const {
  std::map<std::string, int>::const_iterator attributesIt = int_attributes.find(key);
  return (attributesIt != int_attributes.end()) ? (*attributesIt).second : 0;
}

bool EntityComponent::bool_attribute(const std::string& key) const {
  std::map<std::string, bool>::const_iterator attributesIt = bool_attributes.find(key);
  return (attributesIt != bool_attributes.end()) ? (*attributesIt).second : 0;  
}