#ifndef CubeFX_ModelResource_h
#define CubeFX_ModelResource_h

#include "Standard.h"

class MeshResource;

class ModelResource {
  
public:
  
  static ModelResource* resource();
  
  ModelResource() { };
  
  void load(const std::string& file);
  
  inline 
  std::deque<MeshResource*> meshes() const { return meshes_; };
  
private:
  
  std::deque<MeshResource*> meshes_;
    
};

#endif
