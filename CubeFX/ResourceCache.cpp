#include "ResourceCache.h"

#include "File.h"
#include "ModelResource.h"
#include "ShaderResource.h"
#include "TextureResource.h"

ModelResource* ResourceCache::load_model(const std::string& resource_file) {
  ModelResource* resource = ModelResource::resource();
  resource->load(resource_file);
  return resource;
}

ShaderResource* ResourceCache::load_shader(const std::string& resource_file) {
  ShaderResource* resource = ShaderResource::resource();
  resource->load("shader.vsh", "shader.fsh");
  return resource;
}

FontDescriptorResource* ResourceCache::load_font_descriptor(const std::string& resource_file) {
  FontDescriptorResource* resource = FontDescriptorResource::resource();
  resource->load(resource_file);
  return resource;
}

TextureResource* ResourceCache::load_texture(const std::string& resource_file) {
  TextureResource* resource = TextureResource::resource();
  resource->load(resource_file);
  return resource;
}