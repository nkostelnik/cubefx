#ifndef CubeFX_VisibleEntityUpdateJob_h
#define CubeFX_VisibleEntityUpdateJob_h

#include "IJob.h"

class EntityComponent;
class GameScreen;

class VisibleEntityUpdateJob : public IJob {
  
public:
  
  static VisibleEntityUpdateJob* job(GameScreen& game_screen, const EntityComponent* component);
  
  VisibleEntityUpdateJob(GameScreen& game_screen, const EntityComponent* component)
  : game_screen(game_screen)
  , component(component) { }
  
  void run();
  
private:
  
  const EntityComponent* component;
  GameScreen& game_screen;
  
};

#endif
