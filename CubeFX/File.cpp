#include "File.h"

#include "Standard.h"
#include "Platform.h"

#include <fstream>
#include <sstream>

std::string File::data() {
  std::string full_path = Platform::path_for_file(file_path.c_str());
  std::ifstream file_stream(full_path.c_str());
  if (!file_stream) {
    LOG("Failed to open file %s", full_path.c_str());
  }
  std::stringstream data;
  data << file_stream.rdbuf();
  return data.str();
}

