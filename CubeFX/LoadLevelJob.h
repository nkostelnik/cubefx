#ifndef CubeFX_LoadLevelJob_h
#define CubeFX_LoadLevelJob_h

#include "IJob.h"
#include "Standard.h"

class EntitySystem;
class EntityManagement;

class LoadLevelJob : public IJob {
  
public:
  
  static LoadLevelJob* job(const std::string& level_name, EntityManagement& entity_management);
  
  LoadLevelJob(const std::string& level_name, EntityManagement& entity_management) 
  : level_name(level_name), entity_management(entity_management) { };
  
  void run();
  
private:
  
  std::string level_name;
  EntityManagement& entity_management;
  
};

#endif
