#ifndef EntitySystem_h
#define EntitySystem_h

#include "Standard.h"
#include <deque>

#include "EntityComponent.h"
class EntitySystem {
  
public:
  
  static EntitySystem* instance();
  
  void add_component(EntityComponent* component);
  
  std::deque<EntityComponent*> get_components(const char* type) const;
  
  EntityComponent* get_component(INT id, const char* type) const;
  
private:
  
  std::deque<EntityComponent*> components;
  
  static EntitySystem* _instance;
  
};

#endif