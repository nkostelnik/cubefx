#ifndef CubeFX_Texture_h
#define CubeFX_Texture_h

#include "Standard.h"

class TextureResource;
class Shader;

class Texture {
  
public:
  
  static Texture* texture(TextureResource* texture_resource);
  
  void render(const Shader &shader);
  
private:
  
  Texture(TextureResource* texture_resource) 
    : texture_id(INVALID)
    , texture_resource(texture_resource) { };
  
  UINT texture_id;
  TextureResource* texture_resource;
  
};

#endif
