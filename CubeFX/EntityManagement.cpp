#include "EntityManagement.h"

#include "VisibleEntityManager.h"
#include "SpatialEntityManager.h"
#include "LightEntityManager.h"
#include "CameraEntityManager.h"

#include "EntitySetupJob.h"
#include "JobQueue.h"

EntityManagement::EntityManagement(GameScreen& game_screen) {
  managers.push_back(SpatialEntityManager::manager());
  managers.push_back(VisibleEntityManager::manager(game_screen));
  managers.push_back(LightEntityManager::manager(game_screen));
  managers.push_back(CameraEntityManager::manager(game_screen));
}

void EntityManagement::init() {
  std::deque<EntityManager*>::iterator managersIt = managers.begin();
  for (; managersIt != managers.end(); ++managersIt) {
    EntityManager* manager = (*managersIt);
    manager->init();
  }
}

void EntityManagement::setup() {
  std::deque<EntityManager*>::iterator managersIt = managers.begin();
  for (; managersIt != managers.end(); ++managersIt) {
    EntityManager* manager = (*managersIt);
    EntitySetupJob* job = EntitySetupJob::job(manager);
    JobQueue::instance()->queue_job(job);
  }
}

