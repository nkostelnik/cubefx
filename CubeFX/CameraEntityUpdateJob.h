#ifndef CubeFX_CameraEntityUpdateJob_h
#define CubeFX_CameraEntityUpdateJob_h

#include "IJob.h"

class EntityComponent;
class GameScreen;

class CameraEntityUpdateJob : public IJob {
  
public:
  
  static CameraEntityUpdateJob* job(GameScreen& game_screen, const EntityComponent* component, const EntityComponent* spatial);
  
  CameraEntityUpdateJob(GameScreen& game_screen, const EntityComponent* component, const EntityComponent* spatial)
  : game_screen(game_screen)
  , component(component) 
  , spatial(spatial) { }
  
  void run();
  
private:
  
  const EntityComponent* component;
  const EntityComponent* spatial;
  
  GameScreen& game_screen;
  
};


#endif
