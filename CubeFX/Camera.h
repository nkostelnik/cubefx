#ifndef CubeFX_Camera_h
#define CubeFX_Camera_h

#include "Standard.h"

class Camera {
  
public:
  
  static Camera* camera(INT id);
  
  bool is_active();
  
  void set_active(bool active);
  
  INT id;
  glm::vec3 position;
  
private:
  
  Camera(INT id) 
    : id(id)
    , is_active_(false) 
  { };
  
  bool is_active_;
  
};

#endif
