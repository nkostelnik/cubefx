#ifndef CubeFX_Material_h
#define CubeFX_Material_h

#include "Standard.h"

class Shader;
class Texture;
class MaterialResource;

class Material {
  
public:
  
  static Material* material(MaterialResource* material_resource);
  
  void render(const Shader& shader);
  
private:
  
  Material(MaterialResource* material_resource);
  
  glm::vec4 ambient;
  glm::vec4 diffuse;
  glm::vec4 specular;
  
  std::deque<Texture*> textures;
  
};

#endif
