#include "LevelFile.h"

#include "Standard.h"
#include "File.h"
#include "json/reader.h"
#include "EntityComponent.h"

using namespace json;

std::deque<EntityComponent*> LevelFile::components() {
  std::deque<EntityComponent*> components;
  INT component_id = 0;
  
  File file(file_path);
  std::stringstream data;
  data << file.data();
  
  Object rootObject;
  Reader::Read(rootObject, data);
  
  const Array& jsonEntities = rootObject["entities"];

  Array::const_iterator itEntities = jsonEntities.Begin();
  for (; itEntities != jsonEntities.End(); ++itEntities) {
    
    ++component_id;
    
    const Array& jsonComponents = (*itEntities)["components"];
    Array::const_iterator itComponents = jsonComponents.Begin();
    for (; itComponents != jsonComponents.End(); ++itComponents) {
      
      const Object& component_object = *itComponents;
      Object::const_iterator itComponentValues(component_object.Begin());
      
      EntityComponent* component = EntityComponent::component(component_id);
      
      for (; itComponentValues != component_object.End(); ++itComponentValues) {
        const Object::Member& member = (*itComponentValues);
        const std::string& name = member.name;

        try {
          String value = member.element;
          component->set_attribute(name.c_str(), value.Value());
        } catch(Exception str) { }
        
        try { 
          Number value = member.element;
          component->set_attribute(name.c_str(), value.Value());
        } catch(Exception str) { }
        
        try { 
          Boolean value = member.element;
          component->set_attribute(name.c_str(), value.Value());
        } catch(Exception str) { }

      }
      
      components.push_back(component);
    }
  }
  
  return components;
}