#include "Shader.h"

#include "Light.h"

Shader* Shader::shader() {
  return new Shader();
}

Shader::Shader() {
  program = glCreateProgram();
}

GLuint Shader::compile_shader(const std::string& shader_source, GLint type) {
  GLuint shader = glCreateShader(type);
  const char* source = shader_source.c_str();
  glShaderSource(shader, 1, &source, 0);
  glCompileShader(shader);
  
#if defined(DEBUG)
  GLint logLength;
  glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &logLength);
  if (logLength > 0)
  {
    GLchar *log = (GLchar *)malloc(logLength);
    glGetShaderInfoLog(shader, logLength, &logLength, log);
    LOG("Shader compile log: %s", log);
    free(log);
  }
#endif
  
  GLint status = 0;
  glGetShaderiv(shader, GL_COMPILE_STATUS, &status);
  
  if (status == 0) {
    glDeleteShader(shader);
    LOG("Error compiling shader");
  }

  return shader;
}

void Shader::compile_vertex(const std::string& vertex_source) {
  vertex = compile_shader(vertex_source, GL_VERTEX_SHADER);
}

void Shader::compile_fragment(const std::string& fragment_source) {
  fragment = compile_shader(fragment_source, GL_FRAGMENT_SHADER);
}

void Shader::bind_attribute(INT attribute_id, const char* attribute_name) {
  glBindAttribLocation(program, attribute_id, attribute_name);
}

void Shader::link() {
  glAttachShader(program, vertex);
  glAttachShader(program, fragment);
  
  GLint status;
  glLinkProgram(program);
#if defined(DEBUG)
  GLint logLength;
  glGetProgramiv(program, GL_INFO_LOG_LENGTH, &logLength);
  if (logLength > 0)
  {
    GLchar *log = (GLchar *)malloc(logLength);
    glGetProgramInfoLog(program, logLength, &logLength, log);
    LOG("Program link log:\n%s", log);
    free(log);
  }
#endif
  
  glGetProgramiv(program, GL_LINK_STATUS, &status);
  if (status == 0) {
    LOG("Error linking shader program");
  }
}

void Shader::use() const {
  glUseProgram(program);
}

void Shader::set_uniform(const glm::vec4& uniform_data, const char* uniform_name) const {
  std::map<std::string, GLint>::const_iterator uniformIt = uniforms.find(uniform_name);
  if (uniformIt != uniforms.end()) {
    GLint uniform_id = (*uniformIt).second;
    glUniform4fv(uniform_id, 1, glm::value_ptr(uniform_data));
  }    
}

void Shader::set_uniform(const glm::vec3& uniform_data, const char* uniform_name) const {
  std::map<std::string, GLint>::const_iterator uniformIt = uniforms.find(uniform_name);
  if (uniformIt != uniforms.end()) {
    GLint uniform_id = (*uniformIt).second;
    glUniform3fv(uniform_id, 1, glm::value_ptr(uniform_data));
  }    
}

void Shader::set_uniform(const glm::mat3& uniform_data, const char* uniform_name) const {
  std::map<std::string, GLint>::const_iterator uniformIt = uniforms.find(uniform_name);
  if (uniformIt != uniforms.end()) {
    GLint uniform_id = (*uniformIt).second;
    glUniformMatrix3fv(uniform_id, 1, GL_FALSE, glm::value_ptr(uniform_data));
  }
}

void Shader::set_uniform(const glm::mat4& uniform_data, const char* uniform_name) const {
  std::map<std::string, GLint>::const_iterator uniformIt = uniforms.find(uniform_name);
  if (uniformIt != uniforms.end()) {
    GLint uniform_id = (*uniformIt).second;
    glUniformMatrix4fv(uniform_id, 1, GL_FALSE, glm::value_ptr(uniform_data));
  }
}

void Shader::add_uniform(const char* uniform_name) {
  GLint uniform_id = glGetUniformLocation(program, uniform_name);
  uniforms.insert(std::make_pair(uniform_name, uniform_id));
}

void Shader::set_uniform(INT uniform_data, const char* uniform_name) const {
  std::map<std::string, GLint>::const_iterator uniformIt = uniforms.find(uniform_name);
  if (uniformIt != uniforms.end()) {
    GLint uniform_id = (*uniformIt).second;
    glUniform1i(uniform_id, uniform_data);
  }  
}

void Shader::set_uniform(const std::deque<Light*>& uniform_data, const char* uniform_name) const {
  std::map<std::string, GLint>::const_iterator uniformIt = uniforms.find(uniform_name);
  if (uniformIt != uniforms.end()) {
    float light_positions[uniform_data.size() * 3];
    int index = 0;
    for (Light* light : uniform_data) {
      light_positions[index++] = light->position.x;
      light_positions[index++] = light->position.y;
      light_positions[index++] = light->position.z;
    }    
    
    GLint uniform_id = (*uniformIt).second;
    glUniform3fv(uniform_id, uniform_data.size(), light_positions);
  }
}
