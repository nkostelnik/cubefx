#include "TextureResource.h"

#include "Standard.h"
#include "Platform.h"

TextureResource* TextureResource::resource() {
  return new TextureResource();
}

void TextureResource::load(const std::string& filename) {
  std::string full_path = Platform::path_for_file(filename.c_str());
  Platform::load_image(full_path, &width_, &height_, &data_);
}
