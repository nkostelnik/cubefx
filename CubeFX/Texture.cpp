#include "Texture.h"

#include "TextureResource.h"
#include "Shader.h"

#include <iostream>
#include <fstream>

Texture* Texture::texture(TextureResource* texture_resource) {
  return new Texture(texture_resource);
}


void Texture::render(const Shader &shader) {
  if (texture_id == INVALID) {
    glGenTextures(1, &texture_id);
    glBindTexture(GL_TEXTURE_2D, texture_id);

    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    
    glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
    
    glTexImage2D(GL_TEXTURE_2D, 0, texture_resource->components(), texture_resource->width(), texture_resource->height(), 0, 
                 texture_resource->format(), GL_UNSIGNED_BYTE, texture_resource->data());
    
    glGenerateMipmap(GL_TEXTURE_2D);
  }
 
  glBindTexture(GL_TEXTURE_2D, texture_id);
  shader.set_uniform(0, "colorMap");
}