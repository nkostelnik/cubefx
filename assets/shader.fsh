uniform highp vec4 ambientColor, diffuseColor, specularColor;
varying highp vec3 varyingNormal, varyingLightDir[5];
uniform highp vec3 lightPosition[5];
uniform highp int lightCount;
varying highp vec2 varyingTextureCoords;
uniform sampler2D colorMap;

void main() {
  highp float diffuse = 0.0;
  for (int i = 0; i < lightCount; i++) {
    diffuse += max(0.0, dot(normalize(varyingNormal),
                            normalize(varyingLightDir[i])));      
  }
  
  gl_FragColor = diffuseColor * diffuse;
  
  gl_FragColor += ambientColor;
  
  // gl_FragColor *= texture2D(colorMap, varyingTextureCoords.st);
  
  for (int i = 0; i < lightCount; i++) {
    highp vec3 lightDirection = varyingLightDir[i];
    highp vec3 reflection = normalize(reflect(-normalize(lightDirection), normalize(varyingNormal)));
    highp float spec = max(0.0, dot(normalize(varyingNormal), reflection));
    highp float fSpec = pow(spec, 128.0);
    gl_FragColor.rgb += (vec3(fSpec, fSpec, fSpec)); 
  }
}
