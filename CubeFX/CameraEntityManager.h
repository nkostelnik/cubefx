#ifndef CubeFX_CameraEntityManager_h
#define CubeFX_CameraEntityManager_h

#include "EntityManager.h"

class GameScreen;

class CameraEntityManager : public EntityManager {
  
public:
  
  static CameraEntityManager* manager(GameScreen& game_screen);
  
  CameraEntityManager(GameScreen& game_screen)
  : game_screen(game_screen) { };
  
  void setup();
  
  void init();
  
  void update();
  
private:
  
  GameScreen& game_screen;
  
};
  
#endif
