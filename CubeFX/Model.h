#ifndef CubeFX_Model_h
#define CubeFX_Model_h

#include "Mesh.h"

class Shader;
class ModelResource;

class Model {
  
public:
  
  static Model* model(INT id, ModelResource* model_resource);
    
  void render(const Shader& shader, const glm::mat4& view) const;
  
  glm::vec3 position;
  
  INT id;
  
private:
  
  Model(INT id, ModelResource* model);
  
  std::deque<Mesh> meshes;
  
};

#endif
