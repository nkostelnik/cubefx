#include "CameraEntityUpdateJob.h"

#include "EntityComponent.h"
#include "GameScreen.h"

CameraEntityUpdateJob* CameraEntityUpdateJob::job(GameScreen& game_screen, const EntityComponent* component, const EntityComponent* spatial) {
  return new CameraEntityUpdateJob(game_screen, component, spatial);
}

void CameraEntityUpdateJob::run() {  
  float x = spatial->float_attribute("x");
  float y = spatial->float_attribute("y");
  float z = spatial->float_attribute("z");
  glm::vec3 position(x, y, z);
  game_screen.set_camera_position(component->id(), position);  
}