#ifndef CubeFX_GameScreen_h
#define CubeFX_GameScreen_h

#include "Standard.h"

class Model;
class Light;
class Shader;
class Camera;

class GameScreen {
  
public:
  
  void init();
  
  void add_model(Model* model);
  
  void add_light(Light* light);
  
  void add_camera(Camera* camera);
  
  void set_active_camera(Camera* camera);
  
  void render() const;
  
  void set_light_position(INT id, const glm::vec3& position);
  
  void set_model_position(INT id, const glm::vec3& position);
  
  void set_camera_position(INT id, const glm::vec3& position);
  
  void set_light_color(INT id, const glm::vec3& color);
  
private:
  
  std::deque<Model*> models;
  std::deque<Light*> lights;
  std::deque<Camera*> cameras;
  
  Shader* shader;
  
};

#endif
