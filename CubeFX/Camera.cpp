#include "Camera.h"

Camera* Camera::camera(INT id) {
  return new Camera(id);
}

bool Camera::is_active() { 
  return is_active_;
}

void Camera::set_active(bool active) {
  is_active_ = active;
}