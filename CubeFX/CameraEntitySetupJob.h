#ifndef CubeFX_CameraEntitySetupJob_h
#define CubeFX_CameraEntitySetupJob_h

#include "IJob.h"

class EntityComponent;
class GameScreen;

class CameraEntitySetupJob : public IJob {
  
public:
  
  static CameraEntitySetupJob* job(GameScreen& game_screen, const EntityComponent* component);
  
  CameraEntitySetupJob(GameScreen& game_screen, const EntityComponent* component)
  : game_screen(game_screen)
  , component(component) { }
  
  void run();
  
private:
  
  const EntityComponent* component;
  GameScreen& game_screen;
  
};


#endif
