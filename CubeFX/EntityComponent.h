#ifndef CubeFX_EntityComponent_h
#define CubeFX_EntityComponent_h

#include <map>
#include <string>
#include "Standard.h"

#include "Any.h"

class EntityComponent {
  
public:
  
  static EntityComponent* component(INT component_id);
  
  EntityComponent(INT component_id)
  : component_id(component_id) { };
  
  inline INT id() const { return component_id; };
  
  void set_attribute(const std::string& key, FLOAT value);
  
  void set_attribute(const std::string& key, INT value);
  
  void set_attribute(const std::string& key, bool value);
  
  void set_attribute(const std::string& key, const std::string& value);
    
  std::string string_attribute(const std::string& key) const;
  
  FLOAT float_attribute(const std::string& key) const;
  
  FLOAT int_attribute(const std::string& key) const;
  
  bool bool_attribute(const std::string& key) const;
  
private:
  
  std::map<std::string, std::string> string_attributes;
  std::map<std::string, INT> int_attributes;
  std::map<std::string, FLOAT> float_attributes;
  std::map<std::string, bool> bool_attributes;
  
  INT component_id;
  
};

#endif
