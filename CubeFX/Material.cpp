#include "Material.h"

#include "MaterialResource.h"
#include "Shader.h"
#include "Texture.h"

Material* Material::material(MaterialResource* material_resource) {
  return new Material(material_resource);
}

Material::Material(MaterialResource* material_resource) {
  ambient = material_resource->ambient; 
  diffuse = material_resource->diffuse;
  specular = material_resource->specular;
  
  for (TextureResource* texture_resource : material_resource->textures) {
    Texture* texture = Texture::texture(texture_resource);
    textures.push_back(texture);
  }
}

void Material::render(const Shader &shader) {
  shader.set_uniform(ambient, "ambientColor");
  shader.set_uniform(diffuse, "diffuseColor");
  shader.set_uniform(specular, "specularColor");
  
  for (Texture* texture : textures) {
    texture->render(shader);
  }
}