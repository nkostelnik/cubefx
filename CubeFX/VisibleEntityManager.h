#ifndef CubeFX_VisibleEntityManager_h
#define CubeFX_VisibleEntityManager_h

#include "EntityManager.h"

class GameScreen;
class EntitySystem;

class VisibleEntityManager : public EntityManager {
  
public:
  
  static VisibleEntityManager* manager(GameScreen& game_screen);
  
  VisibleEntityManager(GameScreen& game_screen)
  : game_screen(game_screen) { };
  
  void setup();
  
  void init();
  
  void update();
  
private:
  
  GameScreen& game_screen;
  
};


#endif
