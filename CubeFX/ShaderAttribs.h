#ifndef CubeFX_ShaderAttribs_h
#define CubeFX_ShaderAttribs_h

#include "Standard.h"

extern INT ATTRIB_NORMAL;
extern INT ATTRIB_VERTEX;
extern INT ATTRIB_UV;

#endif
