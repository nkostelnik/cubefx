#include "EntitySetupJob.h"

#include "Standard.h"
#include "EntityManager.h"

EntitySetupJob* EntitySetupJob::job(EntityManager* entity_manager) {
  return new EntitySetupJob(entity_manager);
}

void EntitySetupJob::run() {
  entity_manager->setup();
}