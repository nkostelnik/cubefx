#ifndef CubeFX_EntityManagement_h
#define CubeFX_EntityManagement_h

#include <deque>

class EntityManager;
class GameScreen;

class EntityManagement {
  
public:
  
  EntityManagement() { };
  EntityManagement(GameScreen& game_screen);
  
  void setup();
  
  void init();
  
private:
  
  std::deque<EntityManager*> managers;
  
};

#endif
