#ifndef CubeFX_UI_h
#define CubeFX_UI_h

#include "Standard.h"

class Texture;
class Shader;

class Button;
class Label;

class UI {
  
public:
  
  void init();
  
  void render() const;
  
  void update(float dt);
  
private:
  
  Shader* shader;
  
  std::deque<Button*> buttons;  
  std::deque<Label*> labels;
  
};

#endif
