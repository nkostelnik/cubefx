#ifndef CubeFX_IJob_h
#define CubeFX_IJob_h

class IJob {
  
public:
  
  virtual void run() = 0;
  
};

#endif
