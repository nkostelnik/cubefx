#include "Label.h"

#include "Texture.h"
#include "TextureResource.h"

#include "Shader.h"
#include "ShaderAttribs.h"

#include "FontDescriptor.h"
#include "FontDescriptorResource.h"

#include "ResourceCache.h"

Label* Label::label(const std::string& text, const std::string& bitmap_font_name) {
  Label* label = new Label();
  label->init(bitmap_font_name);
  label->set_text(text);
  return label;
}

void Label::init(const std::string& bitmap_font_name) {
  std::string asset_name = bitmap_font_name + std::string(".png");
  TextureResource* font_texture_resource = ResourceCache::load_texture(asset_name);
  texture_ = Texture::texture(font_texture_resource);
  
  std::string descriptor_name = bitmap_font_name + std::string(".fnt");
  FontDescriptorResource* font_descriptor_resource = ResourceCache::load_font_descriptor(descriptor_name);
  font_descriptor_ = FontDescriptor::descriptor(font_descriptor_resource);
}

void Label::render(const Shader& shader) {  
  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
  texture_->render(shader);
  
  float offset = 0.0f;
  for (char character : text_) {
    FontCharacterInfo character_info = font_descriptor_->char_info(character);
    
    glm::mat4 model = glm::translate(glm::mat4(1.0f), position_);
    shader.set_uniform(model, "model");
    
    float width = character_info.width;
    float height = character_info.height;
    
    float vertices[] = { 
      offset,  height, 0.0f,
      offset,  0, 0.0f, 
      offset + width,  height, 0.0f,
      
      offset + width, height, 0.0f,
      offset, 0.0f, 0.0f,
      offset + width, 0.0f, 0.0f };
    
    glVertexAttribPointer(ATTRIB_VERTEX, 3, GL_FLOAT, 0, 0, vertices);
    glEnableVertexAttribArray(ATTRIB_VERTEX);
    
    float texel_left = (character_info.x-1) / font_descriptor_->width();
    float texel_right = texel_left + (character_info.width / font_descriptor_->width());
   
    float texel_top = 1 - (character_info.y / font_descriptor_->height());
    float texel_bottom = texel_top - (character_info.height / font_descriptor_->height());
    
    float uvs[] = { texel_left, texel_top,
                    texel_left, texel_bottom, 
                    texel_right, texel_top,
                    
                    texel_right, texel_top,
                    texel_left, texel_bottom,
                    texel_right, texel_bottom };
      
    glVertexAttribPointer(ATTRIB_UV, 2, GL_FLOAT, 0, 0, uvs);
    glEnableVertexAttribArray(ATTRIB_UV);
    
    glDrawArrays(GL_TRIANGLES, 0, 6);      
    offset += character_info.width + character_info.offset_x;
  }  
}

void Label::set_position(float x, float y) {
  position_.x = x; 
  position_.y = y;
}

void Label::update(float dt) {
  fps_time_skipped_ += dt;
  if (fps_time_skipped_ >= 0.3f) {
    fps_time_skipped_ = 0.0f;
    std::stringstream fps;
    fps << (INT)(1.0f/dt) << " fps";
    this->set_text(fps.str());
  }
}