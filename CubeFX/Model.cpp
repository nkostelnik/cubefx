#include "Model.h"

#include "Standard.h"

#include "ModelResource.h"
#include "MeshResource.h"
#include "MaterialResource.h"
#include "ShaderResource.h"

#include "Mesh.h"
#include "Material.h"
#include "MatrixStack.h"

Model* Model::model(INT id, ModelResource* model_resource) {
  return new Model(id, model_resource);
}

Model::Model(INT id, ModelResource* model) : id(id) {
  for (MeshResource* mesh_resource : model->meshes()) {
    Mesh mesh(mesh_resource);
    meshes.push_back(mesh);
  }
}

void Model::render(const Shader& shader, const glm::mat4& view) const {
  for (std::deque<Mesh>::const_iterator mesh = meshes.begin(); mesh != meshes.end(); ++mesh) {
    MatrixStack model_stack;
    model_stack.push(glm::translate(glm::mat4(1.0), position));
    (*mesh).render(shader, model_stack, view);
  }
}