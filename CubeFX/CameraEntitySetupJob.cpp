#include "CameraEntitySetupJob.h"

#include "EntityComponent.h"
#include "Camera.h"

#include "GameScreen.h"

CameraEntitySetupJob* CameraEntitySetupJob::job(GameScreen& game_screen, const EntityComponent* component) {
  return new CameraEntitySetupJob(game_screen, component);
}

void CameraEntitySetupJob::run() {
  Camera* camera = Camera::camera(component->id());
  game_screen.add_camera(camera);
  
  bool active = component->bool_attribute("active");
  if (active) {
    game_screen.set_active_camera(camera);
  }
}
