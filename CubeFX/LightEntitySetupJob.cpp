#include "LightEntitySetupJob.h"

#include "EntityComponent.h"
#include "Light.h"

#include "GameScreen.h"

LightEntitySetupJob* LightEntitySetupJob::job(GameScreen& game_screen, const EntityComponent* component) {
  return new LightEntitySetupJob(game_screen, component);
}

void LightEntitySetupJob::run() {
  float r = component->float_attribute("r");
  float g = component->float_attribute("g");
  float b = component->float_attribute("b");
  glm::vec3 color(r, g, b);
  Light* light = Light::light(component->id(), color);
  game_screen.add_light(light);
}
