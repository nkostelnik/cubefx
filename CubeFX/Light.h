#ifndef CubeFX_Light_h
#define CubeFX_Light_h

#include "Standard.h"

class Light {
  
public:
  
  static Light* light(INT id, const glm::vec3& color);
  
  Light(INT id, const glm::vec3& color)
  : id(id), color(color) { }
  
  glm::vec3 color;
  glm::vec3 position;
  INT id;
    
};

#endif
