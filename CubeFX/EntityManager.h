#ifndef CubeFX_EntityManager_h
#define CubeFX_EntityManager_h

class EntityManager {
  
public:
  
  virtual void init() = 0;
    
  virtual void setup() = 0;
  
  virtual void update() = 0;
  
};

#endif
