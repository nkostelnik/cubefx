#ifndef CubeFX_Mesh_h
#define CubeFX_Mesh_h

#include "Standard.h"
#include "Material.h"

class Shader;
class MatrixStack;
class MeshResource;

class Mesh {
  
public:
  
  Mesh(MeshResource* mesh_resource);
  
  void render(const Shader& shader, MatrixStack model_stack, const glm::mat4& view) const;
  
private:

  MeshResource* mesh_resource;
  std::deque<Material*> materials;
  
};


#endif
