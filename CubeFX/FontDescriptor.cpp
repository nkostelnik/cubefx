#include "FontDescriptor.h"

#include "FontDescriptorResource.h"

#include "String.h"

#include <sstream>
#include <iostream>
#include <stdlib.h> 

FontDescriptor* FontDescriptor::descriptor(FontDescriptorResource* font_descriptor_resource) {
  FontDescriptor* descriptor = new FontDescriptor();
  
  std::stringstream data;
  data << font_descriptor_resource->data();
  String line;
  while(std::getline(data, line)) {
    String line_type = line.first_word();
    
    if (line_type.is_equal("common")) {
      std::vector<String> parameters = line.split(' ');
      for (String parameter : parameters) {
        std::vector<String> tokens = parameter.split('=');
        
        if (tokens.front().is_equal("scaleW")) {
          descriptor->width_ = atoi(tokens.back().c_str());
        }
        
        if (tokens.front().is_equal("scaleH")) {
          descriptor->height_ = atoi(tokens.back().c_str());
        }
      }
    }
    
    if (line_type.is_equal("char")) {
      std::vector<String> parameters = line.split(' ');
      
      FontCharacterInfo char_info;
      for (String parameter : parameters) {
        std::vector<String> tokens = parameter.split('=');
        
        if (tokens.front().is_equal("width")) {
          char_info.width = atoi(tokens.back().c_str());
        }
        
        if (tokens.front().is_equal("height")) {
          char_info.height = atoi(tokens.back().c_str());
        }
        
        if (tokens.front().is_equal("x")) {
          char_info.x = atoi(tokens.back().c_str());
        }
        
        if (tokens.front().is_equal("y")) {
          char_info.y = atoi(tokens.back().c_str());
        }
        
        if (tokens.front().is_equal("id")) {
          char_info.char_id = atoi(tokens.back().c_str());
        }

        if (tokens.front().is_equal("xoffset")) {
          char_info.offset_x = atoi(tokens.back().c_str());
        }

        if (tokens.front().is_equal("yoffset")) {
          char_info.offset_y = atoi(tokens.back().c_str());
        }

      }
      
      descriptor->add_char_info(char_info);
    }
  }
  
  return descriptor;
}

void FontDescriptor::add_char_info(const FontCharacterInfo& char_info) {
  char_info_.insert(std::make_pair(char_info.char_id, char_info));
}

FontCharacterInfo FontDescriptor::char_info(char character) {
  if (char_info_.find(character) != char_info_.end()) {
    return char_info_[character];
  }
  return FontCharacterInfo();
}