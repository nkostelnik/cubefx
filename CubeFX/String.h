#ifndef CubeFX_String_h
#define CubeFX_String_h

#import "Standard.h"

class String : public std::string {
  
public:
  
  String() : std::string() { };
  String(const char* string) : std::string(string) { };
  String(const std::string& string) : std::string(string) { }
  
  String first_word() {
    return this->substr(0, this->find(' '));
  }
  
  bool is_equal(const String& other) {
    return this->compare(other) == 0;
  }
  
  std::vector<String> split(char delim) {
    std::vector<String> elems;
    return split(*this, delim, elems);
  }
  
private:
  
  std::vector<String>& split(const String& s, char delim, std::vector<String> &elems) {
    std::stringstream ss(s);
    std::string item;
    while(std::getline(ss, item, delim)) {
      elems.push_back(item);
    }
    return elems;
  }
  
};

#endif
