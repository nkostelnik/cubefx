#include "Mesh.h"

#include "Shader.h"
#include "ShaderAttribs.h"
#include "MatrixStack.h"
#include "MeshResource.h"

Mesh::Mesh(MeshResource* mesh_resource) : mesh_resource(mesh_resource) {
  for (MaterialResource* material_resource : mesh_resource->materials) {
    Material* material = Material::material(material_resource);
    materials.push_back(material);
  }
}

void Mesh::render(const Shader& shader, MatrixStack model_stack, const glm::mat4& view) const {
  model_stack.push(glm::scale(glm::mat4(1.0f), mesh_resource->scale));
  
  glm::mat4 rotation_x = glm::rotate(glm::mat4(1.0f), mesh_resource->rotation.x, glm::vec3(1, 0, 0));
  glm::mat4 rotation_y = glm::rotate(glm::mat4(1.0f), mesh_resource->rotation.y, glm::vec3(0, 1, 0));
  glm::mat4 rotation_z = glm::rotate(glm::mat4(1.0f), mesh_resource->rotation.z, glm::vec3(0, 0, 1));
  glm::mat4 rotation = rotation_z * rotation_y * rotation_x;
  model_stack.push(rotation);
  
  glm::mat4 trans = glm::translate(glm::mat4(1), mesh_resource->translation);
  model_stack.push(trans);
  
  glm::mat4 model = model_stack.concatenate();
  
  shader.set_uniform(view * model, "modelview");
  shader.set_uniform(glm::inverseTranspose(glm::mat3(view * model)), "normalMatrix");

  for(Material* material : materials) {
    material->render(shader);
  }
  
  glVertexAttribPointer(ATTRIB_VERTEX, mesh_resource->vertex_size, GL_FLOAT, 0, 0, mesh_resource->vertices);
  glEnableVertexAttribArray(ATTRIB_VERTEX);
  
  glVertexAttribPointer(ATTRIB_NORMAL, mesh_resource->normal_size, GL_FLOAT, 0, 0, mesh_resource->normals);
  glEnableVertexAttribArray(ATTRIB_NORMAL);

  glVertexAttribPointer(ATTRIB_UV, mesh_resource->uv_size, GL_FLOAT, 0, 0, mesh_resource->uvs);
  glEnableVertexAttribArray(ATTRIB_UV);
  
  glDrawArrays(GL_TRIANGLES, 0, mesh_resource->vertex_count);    
}
