#ifndef CubeFX_VisibleEntitySetupJob_h
#define CubeFX_VisibleEntitySetupJob_h

#include "IJob.h"

class EntityComponent;
class GameScreen;

class VisibleEntitySetupJob : public IJob {
  
public:
  
  static VisibleEntitySetupJob* job(GameScreen& game_screen, const EntityComponent* component);
  
  VisibleEntitySetupJob(GameScreen& game_screen, const EntityComponent* component)
  : game_screen(game_screen)
  , component(component) { }
  
  void run();
  
private:
  
  const EntityComponent* component;
  GameScreen& game_screen;
  
};

#endif
