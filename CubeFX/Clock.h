#ifndef CubeFX_Clock_h
#define CubeFX_Clock_h

class Clock {
  
public:
  
  Clock() : first_run_(true) { };
  
  float delta_time();
  
private:
  
  bool first_run_;
  
};

#endif
