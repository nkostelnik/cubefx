#include "JobWorker.h"

#include "JobQueue.h"
#include "IJob.h"

void* JobWorker::worker(void* args) {
  JobWorkerArgs* worker_args = (JobWorkerArgs*)args;
  JobWorker worker(worker_args->queue, worker_args->id);
  worker.start_work();
  return 0;
}

void JobWorker::start_work() {  
  LOG("JobWorker %d started work", id);
  
  while(working) {
    IJob* job = queue->next_job();
    if (job) {
      job->run(); 
      delete job;
    }
  }
  
  LOG("JobWorker %d stopped work", id);
}
